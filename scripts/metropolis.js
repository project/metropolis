// $Id$
Drupal.behaviors.metropolisBehavior = function(context) {

  jQuery('#superfish ul.menu').superfish({
            delay:       250,                           
            animation:   {opacity:'show',height:'show'},  
            speed:       'fast',                          
            autoArrows:  false,                           
            dropShadows: false  
	});
	
	if ($.browser.msie && ($.browser.version < 7)) {
    $('#superfish li .nolink').hover(function() {
      $(this).addClass('hover');
      }, function() {
        $(this).removeClass('hover');
    });
  };

};